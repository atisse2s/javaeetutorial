<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="java.util.Date"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>My Jsp Site</title>
</head>
<body>

<%! int counter = 0; %>

<% counter++; %>

<%= counter %>

<!-- Html-Kommentar -->
<%-- Jsp-Kommentar --%>
<br>
<%= new Date() %>

</body>
</html>