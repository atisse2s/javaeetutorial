<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<c:set var="meineVariable" scope="page" value="Der Wert meiner Variablen"></c:set>
	${meineVariable} <br>
<c:set var="gehalt" scope="page" value="3000"></c:set>

<c:if test="${gehalt > 2500}">
Du verdienst viel!
</c:if>

<br>
<table>
<c:forEach var="i" begin="1" end="10">

<c:choose>
<c:when test="${i%2 != 0}">
<tr><td style="background-color: lightgrey;">
</c:when>

<c:otherwise>
<tr><td style="background-color: white;">
</c:otherwise>


</c:choose>
Nummer: ${i} </td></tr>
</c:forEach>
</table>

</body>
</html>