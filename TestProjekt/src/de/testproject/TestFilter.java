package de.testproject;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

/**
 * Servlet Filter implementation class TestFilter
 */
@WebFilter(
		urlPatterns = { "/TestServlet" }, 
		initParams = { 
				@WebInitParam(name = "color", value = "red")
		})
public class TestFilter implements Filter {

    private FilterConfig filterConfig;

	/**
     * Default constructor. 
     */
    public TestFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		response.getWriter().append("<div style=\"color:").append(filterConfig.getInitParameter("color")).append(";\">");
		chain.doFilter(request, response);
		response.getWriter().append("</div>");
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {

		this.filterConfig = fConfig;
	}

}
