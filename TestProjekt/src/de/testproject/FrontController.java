package de.testproject;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FrontController
 */
@WebServlet("/FrontController")
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FrontController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String cmd = request.getParameter("cmd");
		if("testservlet".equals(cmd)) {
			RequestDispatcher rd = request.getServletContext().getRequestDispatcher("/TestServlet");
			rd.forward(request, response);
		}
		
		if("testservletinclude".equals(cmd)) {
			RequestDispatcher rd = request.getServletContext().getRequestDispatcher("/TestServlet");
			rd.include(request, response);
			response.getWriter().append("<br>Wieder im FrontController");
		}
		
		if("testservletuebernahme".equals(cmd)) {
			RequestDispatcher rd = request.getServletContext().getNamedDispatcher("TestServlet");
			rd.forward(request, response);
		}

		if("testservletueberrequest".equals(cmd)) {
			RequestDispatcher rd = request.getRequestDispatcher("TestServlet");
			rd.forward(request, response);
		}

		if("download".equals(cmd)) {
			RequestDispatcher rd = request.getServletContext().getNamedDispatcher("DownloadServlet");
			rd.forward(request, response);
		}
		
		if("jspdispatch".equals(cmd)) {
			request.setAttribute("name", "Harald");
			RequestDispatcher rd = request.getServletContext().getRequestDispatcher("/mypage.jsp");
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
