package de.testproject;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * 
 *  Application Lifecycle Listener implementation class LifeCycleListener
 *  
 *  Diese Klasse wird bei start und stop server aufgerufen.
 *
 */
@WebListener
public class LifeCycleListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public LifeCycleListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent sce)  { 
    	System.out.println("---------------------");
    	System.out.println("Kontext zerst�rt");
    	System.out.println("---------------------");
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent sce)  { 
    	
    	System.out.println("---------------------");
    	System.out.println("Kontext initialisiert");
    	System.out.println("---------------------");
    }
	
}
