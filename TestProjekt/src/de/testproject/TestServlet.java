package de.testproject;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet(
		description = "Unser erstes servlet", 
		urlPatterns = { "/TestServlet" },
		name = "TestServlet",
		initParams = { 
				@WebInitParam(name = "testparameter", value = "testvalue", description = "Dies ist mein erster Parameter")
		})
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Enumeration<String> initParameters = this.getInitParameterNames();
		
		while(initParameters.hasMoreElements()) {
			String nextParam = initParameters.nextElement();
			String initParameter = this.getInitParameter(nextParam);
			
			response.getWriter().append("Init Parameter: " + nextParam + " :: " + initParameter);
			response.getWriter().append("<br>");
		}

		Enumeration<String> parameterNames = request.getParameterNames();
		
		while(parameterNames.hasMoreElements()) {
			String nextParameterName = parameterNames.nextElement();
			String paramValue = request.getParameter(nextParameterName);
			
			response.getWriter().append("Parameter Name: " + nextParameterName + " Paramter Value: " + paramValue);
			response.getWriter().append("<br>");
			
		}
		
		request.setAttribute("Attribute01", "myAttributeValue");
		Enumeration<String> attributeNames = request.getAttributeNames();
		
		while (attributeNames.hasMoreElements()) {
			String nextAttributeName = (String) attributeNames.nextElement();
			String atrributeValue = request.getAttribute(nextAttributeName).toString();
			response.getWriter().append("Attribute Name: " + nextAttributeName + " Attribute Value: " + atrributeValue);
			response.getWriter().append("<br>");
		}
		
		Enumeration<String> headerNames = request.getHeaderNames();
		
		while (headerNames.hasMoreElements()) {
			String nextHeaderName = (String) headerNames.nextElement();
			String headerValue = request.getHeader(nextHeaderName);
			
			response.getWriter().append("Header Name: " + nextHeaderName + " Header Value: " + headerValue);
			response.getWriter().append("<br>");
		}
		
		request.getServletContext().setAttribute("derName", "Max Mustermann");
		response.getWriter().append("Served at: ").append(request.getContextPath());
		response.getWriter().append("<br>");
		response.getWriter().append("ServletPath at: ").append(request.getServletPath());
		response.getWriter().append("<br>");
		response.getWriter().append("AuthType at: ").append(request.getAuthType());
		response.getWriter().append("<br>");
		response.getWriter().append("PathInfo at: ").append(request.getPathInfo());
		response.getWriter().append("<br>");
		response.getWriter().append("URL: ").append(request.getRequestURL()+"?"+request.getQueryString());
		response.getWriter().append("<br>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
