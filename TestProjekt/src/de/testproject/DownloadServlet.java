package de.testproject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class DownloadServlet
 */
@WebServlet(
		urlPatterns = { "/DownloadServlet" },
		name = "DownloadServlet")
public class DownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DownloadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		File file = new File("G:\\Developement\\Udemy\\JEE_Servlets_JSP\\TestProjekt\\WebContent\\testfile.txt");
		response.setContentType("application/octet-stream");
		response.setContentLengthLong(file.length());
		response.setHeader("Content-Disposition", String.format("attachement; filename=\"%s\"", file.getName()));
		
		OutputStream output = response.getOutputStream();
		
		try (FileInputStream in = new FileInputStream(file)){
			
			byte[] buffer = new byte[4096];
			int length;
			while((length = in.read(buffer)) > 0) {
				output.write(buffer,0,length);
			}
		}
		
		output.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
