package controller.command;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Adresse;
import persistence.AdresseDAO;

public class Create implements Command {

	@Override
	public void executeGET(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(JSPPATH+"create.jsp");
		dispatcher.include(request, response);
	}

	@Override
	public void executePOST(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Adresse neueAdresse = new Adresse (request.getParameter("vorname"),request.getParameter("nachname"), request.getParameter("strasse"), request.getParameter("hausnummer"),request.getParameter("plz"), request.getParameter("ort"));

		try {
			((AdresseDAO) request.getServletContext().getAttribute("adresseDAO")).addAdresse(neueAdresse);
		} catch (SQLException e) {
			throw new ServletException(e);
		}
		
		response.sendRedirect("read.html");
	}

	@Override
	public String getTitel() {
		return "Adresse anlegen";
	}

}
