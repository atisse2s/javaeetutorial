package controller.command;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Adresse;
import persistence.AdresseDAO;

public class Read implements Command {

	@Override
	public void executeGET(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		try {
			ArrayList<Adresse> adressen = ((AdresseDAO) request.getServletContext().getAttribute("adresseDAO")).findAll();
			
			request.setAttribute("adressen", adressen);

			RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(JSPPATH+"read.jsp");
			dispatcher.include(request, response);

		} catch (SQLException e) {
			throw new ServletException(e);
		}
		

	}

	@Override
	public void executePOST(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getTitel() {
		return "Adressliste";
	}

}
