package controller.command;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Adresse;
import persistence.AdresseDAO;

public class Update implements Command {

	@Override
	public void executeGET(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			request.setAttribute("adresse", ((AdresseDAO) request.getServletContext().getAttribute("adresseDAO")).findById(request.getParameter("id")));
		} catch (SQLException e) {
			throw new ServletException(e);
		}
		
		RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher(JSPPATH+"update.jsp");
		dispatcher.include(request, response);

	}

	@Override
	public void executePOST(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Adresse aktualisierteAdresse = new Adresse (request.getParameter("vorname"),request.getParameter("nachname"), request.getParameter("strasse"), request.getParameter("hausnummer"),request.getParameter("plz"), request.getParameter("ort"));
		
		aktualisierteAdresse.setId(new Integer(request.getParameter("id")));
		
		try {
			((AdresseDAO) request.getServletContext().getAttribute("adresseDAO")).setAdresse(aktualisierteAdresse);
		} catch (SQLException e) {
			throw new ServletException(e);
		}
		
		response.sendRedirect("read.html");

	}

	@Override
	public String getTitel() {
		return "Adresse aktualisieren";
	}

}
