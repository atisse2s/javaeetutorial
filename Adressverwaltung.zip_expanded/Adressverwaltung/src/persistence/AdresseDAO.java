package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Adresse;

public class AdresseDAO {

	Connection conn;
	Statement s;

	private String connectionURL = "jdbc:derby:c://temp//Adressverwaltung;create=true";

	public AdresseDAO() {

		try {
			conn = DriverManager.getConnection(connectionURL);
			s = conn.createStatement();
			
			if (!tableExists(conn, "ADRESSEN")) {
				System.out.println("Tabelle anlegen, da nicht vorhanden.");
				

				s.execute( "CREATE TABLE ADRESSEN (ID INT NOT NULL GENERATED ALWAYS AS IDENTITY CONSTRAINT ADRESSEN_PK PRIMARY KEY, "
								+ "VORNAME VARCHAR(32), " + "NAME VARCHAR(32), " + "STRASSE VARCHAR(32), "
								+ "HAUSNUMMER VARCHAR(6), " + "PLZ VARCHAR(5), " + "ORT VARCHAR(32))");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Adresse> findAll() throws SQLException {

		ArrayList<Adresse> result = new ArrayList<Adresse>();

		ResultSet rs = s.executeQuery("select ID, VORNAME, NAME, STRASSE, HAUSNUMMER, PLZ, ORT from ADRESSEN");

		while (rs.next()) {
			Adresse adresse = new Adresse();
			adresse.setId(rs.getInt(1));
			adresse.setVorname(rs.getString(2));
			adresse.setNachname(rs.getString(3));
			adresse.setStrasse(rs.getString(4));
			adresse.setHausnummer(rs.getString(5));
			adresse.setPlz(rs.getString(6));
			adresse.setOrt(rs.getString(7));
			
			result.add(adresse);
		}

		rs.close();

		return result;
	}
	
	public Adresse findById(String parameter) throws SQLException {

		PreparedStatement psfindById = conn
				.prepareStatement("select ID, VORNAME, NAME, STRASSE, HAUSNUMMER, PLZ, ORT from ADRESSEN WHERE id = ?");

		psfindById.setString(1, parameter);
		ResultSet rs = psfindById.executeQuery();
		
		rs.next();
		
		Adresse adresse = new Adresse();
		adresse.setId(rs.getInt(1));
		adresse.setVorname(rs.getString(2));
		adresse.setNachname(rs.getString(3));
		adresse.setStrasse(rs.getString(4));
		adresse.setHausnummer(rs.getString(5));
		adresse.setPlz(rs.getString(6));
		adresse.setOrt(rs.getString(7));

		psfindById.close();

		return adresse;

	}

	public void addAdresse(Adresse adresse) throws SQLException {

		PreparedStatement psInsert = conn.prepareStatement(
				"insert into ADRESSEN (VORNAME, NAME, STRASSE, HAUSNUMMER, PLZ, ORT) values (?, ?, ?, ?, ?, ?)");
		
		psInsert.setString(1,adresse.getVorname());
		psInsert.setString(2,adresse.getNachname());
		psInsert.setString(3,adresse.getStrasse());
		psInsert.setString(4,adresse.getHausnummer());
		psInsert.setString(5,adresse.getPlz());
		psInsert.setString(6,adresse.getOrt());

		psInsert.executeUpdate(); 
		
		psInsert.close();

	}

	public void setAdresse(Adresse adresse) throws SQLException {

		PreparedStatement psUpdate = conn.prepareStatement(
				"update ADRESSEN set VORNAME = ?, NAME = ?, STRASSE = ?, HAUSNUMMER = ?, PLZ = ?, ORT = ? where ID = ?");
		
		psUpdate.setString(1,adresse.getVorname());
		psUpdate.setString(2,adresse.getNachname());
		psUpdate.setString(3,adresse.getStrasse());
		psUpdate.setString(4,adresse.getHausnummer());
		psUpdate.setString(5,adresse.getPlz());
		psUpdate.setString(6,adresse.getOrt());
		psUpdate.setInt(7, adresse.getId());
        
		psUpdate.executeUpdate(); 

		psUpdate.close();
	}

	public void deleteAdresse(Adresse adresse) throws SQLException {
		
		PreparedStatement psDelete = conn.prepareStatement(
				"delete from ADRESSEN where ID = ?");
		
		psDelete.setInt(1, adresse.getId());
        
		psDelete.executeUpdate(); 

		psDelete.close();
	}

	private static boolean tableExists(Connection conn, String tabellenName) throws SQLException {
		PreparedStatement s = null;
		ResultSet r = null;
		try {
			s = conn.prepareStatement("SELECT COUNT(*) FROM " + tabellenName + " WHERE 0 = 1");
			r = s.executeQuery();
			return true; // Tabelle existiert, da wir 0 Zeilen erhalten haben!
		} catch (SQLException e) {
			return false; // Ein Problem trat beim Zugriff auf die Tabelle auf,
							// weil sie nicht existiert.
		} finally {
			if (r != null) {
				r.close();
			}
			if (s != null) {
				s.close();
			}
		}
	}

	public void closeConnection() {
		try {
			s.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws SQLException {
		AdresseDAO adresseDAO = new AdresseDAO();

		// adresseDAO.addAdresse(new Adresse("Max","Mustermann","Musterstraße","11","12345","Entenhausen"));
		
		System.out.println(adresseDAO.findAll().size());
		
		// adresseDAO.deleteAdresse(adresseDAO.findAll().get(0));
		
		Adresse adr = adresseDAO.findAll().get(0);
		
		adr.setVorname("Maria");
		
		adresseDAO.setAdresse(adr);
		
		adresseDAO.closeConnection();
	}

	
}
