<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="now" class="java.util.Date" />
<c:set var="starttime" scope="request" value="${now}"/>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>${titel}</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
<header><h1>${titel}</h1></header>
<article>
