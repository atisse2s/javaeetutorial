<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<table>
<tr>
<th>Vorname</th><th>Nachname</th><th>Strasse</th><th>Hausnummer</th><th>PLZ</th><th>Ort</th><th><strong>Aktionen</strong></th>
</tr>

<c:forEach items="${adressen}" var="adresse"> 
  <tr>
    <td>${adresse.vorname}</td>
    <td>${adresse.nachname}</td>
    <td>${adresse.strasse}</td>
    <td>${adresse.hausnummer}</td>
    <td>${adresse.plz}</td>
    <td>${adresse.ort}</td>
    <td><a href="update.html?id=${adresse.id}">Editieren</a>, <a class="danger" href="delete.html?id=${adresse.id}">L�schen</a></td>
  </tr>
</c:forEach>

</table>
<p>
<a href="create.html">Neue Adresse anlegen</a>
</p>