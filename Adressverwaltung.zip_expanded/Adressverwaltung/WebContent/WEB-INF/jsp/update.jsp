<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<form action="update.html" method="POST">
	<table>
		<tr>
			<td><label for="vn">Vorname</label></td>
			<td><input type="text" name="vorname" id="vn" value="${adresse.vorname}"></td>
		</tr>
		<tr>
			<td><label for="nn">Nachname</label></td>
			<td><input type="text" name="nachname" id="nn" value="${adresse.nachname}"></td>
		</tr>
		<tr>
			<td><label for="st">Strasse</label></td>
			<td><input type="text" name="strasse" id="st" value="${adresse.strasse}"></td>
		</tr>
		<tr>
			<td><label for="nr">Hausnummer</label></td>
			<td><input type="text" name="hausnummer" id="nr" value="${adresse.hausnummer}"></td>
		</tr>
		<tr>
			<td><label for="pl">PLZ</label></td>
			<td><input type="text" name="plz" id="pl" value="${adresse.plz}"></td>
		</tr>
		<tr>
			<td><label for="or">Ort</label></td>
			<td><input type="text" name="ort" id="or" value="${adresse.ort}"></td>
		</tr>
		<tr>
			<td>
			    <input type="hidden" name="id" value="${adresse.id}">
				<input type="submit" value="Aktualisieren">
			</td>
			<td></td>
		</tr>
	</table>
</form>
